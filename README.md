# Moved repositories!
New repository can be found at: https://git.violets-purgatory.dev/bingus_violet/Violets-Purgatory

# Violets-Purgatory

Violet's Purgatory is a website filled to the brim with whatever I feel like adding! Currently, the stable version can be found at https://violets-purgatory.dev and the beta (based on the dev branch) can be found at https://beta.violets-purgatory.dev
Although beta probably *isn't* the right term, `dev.violets-purgatory.dev` is just kinda ugly :/

We also have an API, which can be located at https://api.violets-purgatory.dev, which is currently very under developed, but will continue to have updates for features I see fit.

## To-do   
- [ ] Add more content to the socials page
    - [ ] Pull latest Youtube video & display it
    - [ ] Display current Discord Activities
    - [ ] Display current steam game

Completed:


- [x] Stop using Lanyard Web Socket Directly and proxy it through the API (Alternatively, self host the Lanyard API)
- [x] Cut the main CSS file into multiple so that only the nessacary CSS is loaded (Reduces traffic and loading times)
- [x] Add image caching instead of using image proxies (keeps the security benefit and decreases loading times)
- [x] Add code to automatically minify the HTML
- [x] Add random quotes
- [x] Seperate Values from the javascript into their own config for readability
- [x] Add a commit counter